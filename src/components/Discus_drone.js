import React, { useRef,  useEffect } from 'react'
import { useGLTF, useAnimations } from '@react-three/drei'

export default function Model(props) {
  const group = useRef()
  const { nodes, materials, animations } = useGLTF('/discus_drone.glb')
  const { actions } = useAnimations(animations, group)
  useEffect(()=>{
    console.log(actions);
    actions.animation_19.play();
  })
  return (
    <group ref={group} {...props} dispose={null}>
      <group position={[0, -0.67, 0]}>
        <group rotation={[-Math.PI / 2, 0, 0]}>
          <mesh material={materials.mech_tier} geometry={nodes.Mech_Tier3.geometry} material-color='hotpink' />
        </group>
      </group>
      <group rotation={[-Math.PI / 2, 0, 0]}>
        <group rotation={[Math.PI / 2, 0, 0]}>
          <group scale={[0.39, 0.39, 0.39]}>
            <group position={[0, -1.3, 0]}>
              <group name="Bone_Pelvis">
                <primitive object={nodes.Root} />
              </group>
              <skinnedMesh
                material={materials.discus_drone}
                geometry={nodes.Discus_Drone.geometry}
                skeleton={nodes.Discus_Drone.skeleton}
              />
            </group>
          </group>
        </group>
      </group>
    </group>
  )
}

useGLTF.preload('/discus_drone.glb')
