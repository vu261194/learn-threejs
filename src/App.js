import './App.scss';
import React, { useRef, Suspense } from 'react'
// import {CubeTextureLoader} from 'three'
import { Canvas, useFrame } from '@react-three/fiber'
import { OrbitControls, Icosahedron, useTexture, useGLTF, useProgress, Stars, Sky   } from '@react-three/drei'
import LoginType2 from './components/LoginType2/Login2'
import { a, useTransition } from "@react-spring/web";
// import Boy from './components/3D/Boy'
import Robot from './components/3D/Robot-cute';
import {isMobile} from 'react-device-detect';

// function SkyBox() {
//   const { scene } = useThree();
//   const loader = new CubeTextureLoader();
//   // The CubeTextureLoader load method takes an array of urls representing all 6 sides of the cube.
//   const texture = loader.load([
//     "./right.jpg",
//     "./left.jpg",
//     "./top.jpg",
//     "./bottom.jpg",
//     "./front.jpg",
//     "./back.jpg"
//   ]);
//   // Set the scene background property to the resulting texture.
//   scene.background = texture;
//   return null;
// }



function Loader() {
  const { active, progress } = useProgress();
  const transition = useTransition(active, {
    from: { opacity: 1, progress: 0 },
    leave: { opacity: 0 },
    update: { progress: progress*2 },
  });
  return transition(
    ({ progress, opacity }, active) =>
      active && (
        <a.div className='loading' style={{ opacity }}>
          <span>Chờ xíu nhe...</span>
          <div className='loading-bar-container'>
            <a.div className='loading-bar' style={{ width: progress }}></a.div>
          </div>
        </a.div>
      )
  );
}
// function Robot(props) {
//   const group = useRef()
//   // useFrame(() => (group.current.rotation.y += 0.01));
//   const { nodes, materials } = useGLTF('/Eve.glb')
//   return (
//     <group ref={group}>
//       <group  {...props} dispose={null} scale={[0.005, 0.005, 0.005]} position={[1.5, -0.5, 0.5]}> 
//         <mesh
//           material={materials.Eyes}
//           geometry={nodes.Eve_Eyes.geometry}
//           position={[0.42, 147.74, 144.3]}
//           rotation={[-Math.PI / 2, 0, -0.04]}
//           scale={[39.73, 39.73, 39.73]}
//           material-color='cyan'
//         />
//         <group  position={[0.19, 147.74, 149.57]} rotation={[-Math.PI / 2, 0, -0.04]} scale={[39.73, 39.73, 39.73]}>
//           <mesh material={materials['Eve White']} geometry={nodes.Sphere003.geometry} material-color='lightgray'/>
//           <mesh material={materials['Eve Black']} geometry={nodes.Sphere003_1.geometry} material-color='black'/>
//         </group>
//         <mesh
//           material={materials['Eve White']}
//           geometry={nodes.Eve_Body.geometry}
//           position={[-0.61, 88.88, 149.41]}
//           rotation={[-Math.PI / 2, 0, 2.91]}
//           scale={[9.23, 9.23, 9.23]}
//         />
//       </group>
//     </group>
//   )
// }

function LogoTGL(props) {
  const map = useTexture('./shopk_8K_Roughness.jpg')
  const group = useRef()
  // useFrame(() => (group.current.rotation.y += 0.01));
  const { nodes, materials } = useGLTF('/Logo3d2.glb', true)
  return (
    <group position={[0, -1, 0.5]} ref={group} {...props} dispose={null} scale={isMobile ? [0.4, 0.4, 0.4] : [0.5, 0.5, 0.5]}>
      <mesh  material={materials.lambert7SG} geometry={nodes.pPlane6_1.geometry} material-color='red' material-map={map}/>
      <mesh  material={materials.lambert6SG} geometry={nodes.pPlane6_2.geometry} material-color='green' material-map={map} />
      <mesh  material={materials.lambert5SG} geometry={nodes.pPlane6_3.geometry} material-color='blue' material-map={map} />
    </group>
  )
}

function Box() {
  const ref = useRef();
  useFrame(() => (ref.current.rotation.y += 0.02));
  const nomap = useTexture('./1_earth_16k.jpg')
  return (
    <mesh ref={ref} scale={isMobile ? [0.5, 0.5, 0.5] : [1, 1, 1]} position={isMobile ? [0, 0, 0.5] : [0, 0, 0.5]}>
      <Icosahedron position={isMobile ? [-4, 0.5, 0] : [-3, 0.5, 0]}>
        <sphereBufferGeometry args={[0.5, 64, 64]} attach='geometry' ></sphereBufferGeometry>
        <meshStandardMaterial attach='material' map={nomap} color='dimgrey'></meshStandardMaterial>
      </Icosahedron>
      {/* <Icosahedron position={[-2, 0, 0]}>
        <sphereBufferGeometry attach='geometry'></sphereBufferGeometry>
        <meshMatcapMaterial attach='material' matcap={nomap} color='dimgrey'></meshMatcapMaterial>
      </Icosahedron> */}

    </mesh>
  )
}
function Star() {
  const ref = useRef();
  useFrame(() => (ref.current.rotation.y += 0.001));
  return (
    <Stars ref={ref}></Stars>
  )
}
// function Plan(){
//   const [ref] = usePlane(() => ({
//       rotation: [-Math.PI / 2, 0, 0],
//   }));  
//   return(
//     <mesh rotation={[-Math.PI/ 2,0,0]}>
//       <planBufferGeometry attach='geometry' args={[100,100]}></planBufferGeometry>
//       <meshLambertMaterial attach='material' color='lightgray'></meshLambertMaterial>
//     </mesh>
//   )
// }


function App() {
  // const [position, setPosition] = useState({x: 0,y: 0});
  // const handleMouseMove = e => {
  //   e.persist()
  //   setPosition(state => ({...state,x: e.clientX, y: e.clientY}))
  // }
  // const ref = useRef()
  return (
    <div className="App">
      <div className="Content">
        <Loader />
        <Canvas>
          <OrbitControls />
          <Sky></Sky>
          <Star></Star>
          <directionalLight intensity={2}/>
          <ambientLight intensity={2}></ambientLight>
          <spotLight position={[10, 15, 10]} angle={0.3}></spotLight>
          <Suspense fallback={null}>
            <LogoTGL/>
            <Robot />
            <Box />
          </Suspense>
          {/* <SkyBox/> */}
        </Canvas>
        <div className='container'>
          <span>tgl solutions</span>
        </div>
      </div>
      <div className="Login">
        <LoginType2/>
      </div>
    </div>
  );
}

export default App;
